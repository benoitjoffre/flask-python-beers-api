from flask import Flask
from flask import jsonify
from flask import request
from bson.json_util import dumps
from bson.objectid import ObjectId
from flask_pymongo import PyMongo

app = Flask(__name__)


app.config["MONGO_DBNAME"] = "Mybeers"
app.config["MONGO_URI"] = "mongodb://localhost:27017/Mybeers"
mongo = PyMongo(app)


# ADD A BEER
@app.route('/beers', methods=['POST'])
def add_beer():
    data = request.json
    beer_nom = data['beer_nom']
    beer_marque = data['beer_marque']
    beer_type = data['beer_type']
    beer_description = data['beer_description']
    beer_degre = data['beer_degre']
    
    if beer_nom and beer_type and beer_marque and beer_description and beer_degre and request.method == 'POST':
        mongo.db.beers.insert({'nom': beer_nom, 'marque': beer_marque, 'type': beer_type, 'description': beer_description, 'degre': beer_degre })
        resp = jsonify('La bière à été ajoutée avec succès !')
        resp.status_code = 201
    
    return resp


# GET ALL
@app.route('/beers')
def get_all_beers():
    beers = mongo.db.beers.find()
    resp = dumps(beers)
    return resp

# GET ONE BEER
@app.route('/beers/<id>')
def get_beer(id):
    beer = mongo.db.beers.find_one({'_id': ObjectId(id)})
    resp = dumps(beer)
    return resp

# MODIFY A BEER
@app.route('/beers/<id>', methods=['PUT'])
def update_beer(id):
  data = request.json
  beer_nom = data['beer_nom']
  beer_marque = data['beer_marque']
  beer_type = data['beer_type']
  beer_description = data['beer_description']
  beer_degre = data['beer_degre']
  
  if beer_nom and beer_type and beer_marque and beer_description and beer_degre and request.method == 'PUT':
    mongo.db.beers.update_one({'_id': ObjectId(id)}, {'$set': {'nom': beer_nom, 'marque': beer_marque, 'type': beer_type, 'description': beer_description, 'degre': beer_degre}})
    resp = jsonify('La bière à été modifiée avec succès !')
    resp.status_code = 200
    return resp

# DELETE A BEER
@app.route('/beers/<id>', methods=['DELETE'])
def delete_beer(id):
  mongo.db.beers.delete_one({'_id': ObjectId(id)})
  resp = jsonify('La bière à été supprimée avec succès !')
  resp.status_code = 202
  return resp